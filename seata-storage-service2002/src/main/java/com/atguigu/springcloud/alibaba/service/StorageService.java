package com.atguigu.springcloud.alibaba.service;

/**
 * @author yw
 * @create 2021-04-22 14:49
 */
public interface StorageService {

    /**
     * 减库存
     * @param productionId
     * @param count
     */
    void decrease(Long productionId,Integer count);

}
