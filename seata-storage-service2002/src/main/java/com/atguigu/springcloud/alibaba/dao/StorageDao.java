package com.atguigu.springcloud.alibaba.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author yw
 * @create 2021-04-22 14:50
 */
@Mapper
public interface StorageDao {

    /**
     * 减库存
     * @param productId
     * @param count
     */
    int decrease(@Param("productId") Long productId, @Param("count") Integer count);

}
