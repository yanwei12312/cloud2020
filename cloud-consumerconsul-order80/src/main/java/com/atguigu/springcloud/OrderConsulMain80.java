package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author yanwei
 * @date 2021/4/10 - 22:53
 */
@SpringBootApplication
@EnableDiscoveryClient
public class OrderConsulMain80 {

  public static void main(String[] args) {
      SpringApplication.run(OrderConsulMain80.class,args);
  }

}
