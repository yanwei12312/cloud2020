package com.atguigu.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;

/**
 * @author yw
 * @create 2021-04-12 10:24
 */
public interface LoadBalancer {


    //获取eureka上的活着的机器总数
    ServiceInstance instances(List<ServiceInstance> serviceInstanceList);

}
