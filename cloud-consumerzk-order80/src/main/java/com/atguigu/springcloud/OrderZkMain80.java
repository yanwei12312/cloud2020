package com.atguigu.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author yanwei
 * @date 2021/4/10 - 14:55
 */
@SpringBootApplication
@EnableDiscoveryClient
public class OrderZkMain80 {

  public static void main(String[] args) {
    SpringApplication.run(OrderZkMain80.class,args);
  }

}
