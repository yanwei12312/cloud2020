package com.atguigu.springcloud.alibaba.service;

import com.atguigu.springcloud.alibaba.domain.Order;

/**
 * @author yw
 * @create 2021-04-21 9:33
 */
public interface OrderService {

    void create(Order order);

}
