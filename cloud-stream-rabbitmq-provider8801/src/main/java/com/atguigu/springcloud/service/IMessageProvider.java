package com.atguigu.springcloud.service;

/**
 * @author yw
 * @create 2021-04-14 10:08
 */
public interface IMessageProvider {

    public String send();

}
